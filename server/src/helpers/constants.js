// Just a single source of truth for this status & service type
module.exports = {
    serviceTypes: ['windowCleaning', 'standardCleaning', 'premiumCleaning'],
    serviceStatus: ['pending', 'approved', 'rejected', 'completed'],
}